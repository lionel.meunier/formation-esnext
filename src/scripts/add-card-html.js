export class AddCardHtml {

  constructor(card) {
    this.card = card;
  }

  get title() {
    return this.card.name;
  }

  get text() {
    return this.card.text;
  }

  get img() {
    if(this.card && this.card.img) {
      return this.card.img;
    }
    return 'https://wow.zamimg.com/images/hearthstone/backs/original/Card_Back_Default.png';
  }

  createCard() {
    const content = `
        <h2>${this.title}</h2>
        <p>${this.text}</p>
        <img src="${this.img}"/>
    `;
    const div = document.createElement('div');
    div.id = this.card.cardId;
    div.innerHTML = content;
    document.body.append(div);
  }

}
