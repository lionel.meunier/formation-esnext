import {getCards} from './scripts/get-all.js';
import {AddCardHtml} from './scripts/add-card-html.js';
import {HearthstoneApi} from './scripts/api.js';

const hearthstoneApi = new HearthstoneApi();




getCards('Basic', 'Druid').then((cards) => {
  console.log('allCards', cards);
  cards.forEach((card) => {
    const cardHtml = new AddCardHtml(card);
    cardHtml.createCard();
  })
});

